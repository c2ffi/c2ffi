(in-package :c2ffi)

(export '(generate-xml))

(defvar *gccxml-command* "gccxml")

(defun generate-xml (h-file xml-file &optional options)
  "Runs GCC-XML to generate an XML file from a C header file, with compiler options passed from the list `options`.  *Note: This function is currently only available in OpenMCL.*"
  (format *terminal-io* "~&~%Running ~A~%Options: ~A~%Input: ~S~%Output: ~S~%"
	  *gccxml-command* options
	  (namestring (truename h-file))
	  (namestring xml-file))

  #+:openmcl
  (ccl:run-program *gccxml-command*
		   (append options
			   (list (namestring (truename h-file))
				 (str+ "-fxml=" (namestring xml-file))))
		   :output *terminal-io*
		   :error *error-output*)
  #-:openmcl
  (error "Generate-XML is currently only implemented for OpenMCL.  Please run gccxml from the command line on other platforms."))
