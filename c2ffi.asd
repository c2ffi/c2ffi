(defsystem "c2ffi"
  :author "Daniel Dickison <danieldickison@gmail.com>"
  :version "0.2"
  :depends-on ("xmls" "cl-utilities" "iterate" "anaphora")
  :components ((:file "Package")
  	       (:file "String-Utilities"
	       	      :depends-on ("Package"))
  	       (:file "XML-Utilities"
	       	      :depends-on ("Package" "String-Utilities"))
	       (:file "GCC-XML" 
	       	      :depends-on ("Package" "String-Utilities"))
	       (:file "FFI-Generator"
	       	      :depends-on ("Package" "String-Utilities" "XML-Utilities"))
	       (:file "c2ffi"
	       	      :depends-on ("Package" "GCC-XML" "FFI-Generator"))
	       	      
	       #+:cffi
	       (:file "CFFI-Generator"
	       	      :depends-on ("Package" "FFI-Generator"))
	       
	       #+:uffi
	       (:file "UFFI-Generator"
	              :depends-on ("Package" "C-to-FFI"))))
