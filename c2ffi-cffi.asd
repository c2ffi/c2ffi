(defsystem "c2ffi-cffi"
  :author "Daniel Dickison <danieldickison@gmail.com>"
  :version "0.2"
  :depends-on ("c2ffi" "cffi")
  :components ((:file "CFFI-Generator")))
