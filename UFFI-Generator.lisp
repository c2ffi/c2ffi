(in-package :c2ffi)

#|
(defun make-uffi-defs (root)
  "Returns a list of UFFI definitions, in the order of:
   1. typedefs and enums
   2. structs
   3. functions"
  (id-mappings root)
  (iter
    (with ignore-files = (ignore-files root))
    (for node in (node-children root))
    (unless (member (node-attribute "file" node) 
		    ignore-files :test #'equal)
      (case (intern (node-name node) :keyword)
	(:|Function|      (collect (function-generator node)
			   into functions))
	(:|Typedef|       (collect (typedef-generator node)
			   into typedefs))
	(:|Enumeration|   (collect (enum-generator node)
			   into typedefs))
	(:|Struct|        (collect (struct-generator node root)
			   into structs)))
      (finally 
       (format *terminal-io* "~&Generated:~@
                                  ~5@A typedefs and enums~@
                                  ~5@A structs~@
                                  ~5@A functions~%"
	       (length typedefs) (length structs) (length functions))
       (return (nconc typedefs structs functions))))))

(defun ignore-files (root)
  (let ((files (find-in-tree root "File" () :all t)))
    (iter
      (for file in files)
      (when (string-find (node-attribute "name" file)
			 "gccxml")
	(collect (node-attribute "id" file))))))

(defun function-generator (node)
  `(uffi:def-function
       ,(get-name-with-id (node-attribute "id" node))
       ,(iter
	 (for arg in (find-in-tree node "Argument" () :all t))
	 (collect (list (intern (string-upcase (node-attribute "name" arg)))
			(get-type-with-id (node-attribute "type" arg)))))
     :returning ,(get-type-with-id (node-attribute "returns" node))))

(defun typedef-generator (node)
  `(uffi:def-foreign-type
       ,(get-name-symbol (node-attribute "id" node))
       ,(get-type-with-id (node-attribute "type" node))))

(defun enum-generator (node)
  `(uffi:def-enum
       ,(get-name-symbol (node-attribute "id" node))
       ,(iter
	 (for val in (find-in-tree node "EnumValue" () :all t))
	 (collect (list
		   (intern (string-upcase (node-attribute "name" val)))
		   (parse-integer (node-attribute "init" val)))))))

(defun struct-generator (node root)
  `(uffi:def-struct
       ,(get-name-symbol (node-attribute "id" node))
       ,@(iter
	  (for field-id in (split-sequence #\space (node-attribute "members" node)
					   :remove-empty-subseqs t))
	  (for field = (find-in-tree root "Field" `(("id" ,field-id))))
	  (when field
	    (collect (list
		      (intern (string-upcase (node-attribute "name" field)))
		      (get-type-with-id (node-attribute "type" field))))))))
|#