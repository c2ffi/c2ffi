;;; Copyright 2006 Daniel Dickison (danieldickison@cmu.edu)

(in-package :c2ffi)

(export '(generate-ffi-for-files))

#| Test Call -- must first cd into c2ffi source directory.
   (c2ffi:generate-ffi-for-files "Test/*.h"
                           :gcc-options '("-DAPL=1" "-DIBM=0" "-DLIN=0")
                           :in-package :x-plane)
|#

(defun generate-ffi-for-files (h-files
			       &key (generator-class 'cffi-generator)
			            gcc-options
			            in-package)
  "Generates FFI files for each input file designated by `h-files`.  Each input file is first parsed by the GCC-XML program, whose XML output is then parsed by an instance of `generator-class`. *Note: this function is currently only available in OpenMCL.*

* `h-files` -- a list of pathnames or a pathname designator.  If it is a single
    pathname designator, it may include wildcards which will be expanded
    as by `directory` to produce the list of input files.
* `generator-class` -- a symbol specifying a subclass of `ffi-generator`.
    Default: `'cffi-generator`.
* `gcc-options` -- a list of strings to be passed to GCC-XML.
* `in-package` -- a package designator.  If a string designator is used, the
    package need not be currently defined, but must be defined when the FFI
    bindings are loaded.

#### Example ####

The following example parses all .h files in the current directory and outputs CFFI bindings to files in a subdirectory named \"CFFI\".  The compiler option -DMY_DEF=1 is passed to GCC-XML.

    (generate-ffi-for-files \"*.h\"
                            :gcc-options '(\"-DMY_DEF=1\")
                            :in-package :cl-user)"

  (dolist (infile (if (listp h-files)
		      h-files
		      (directory h-files)))
    (let* ((xmlfile (make-pathname 
		     :type "xml"
		     :directory (append (pathname-directory infile) '("XML"))
		     :defaults infile))
	   (generator (make-instance generator-class
				     :xml-file xmlfile)))
      (ensure-directories-exist xmlfile)

      ;; Run GCC-XML.
      (generate-xml infile xmlfile gcc-options)

      ;; Run the FFI generator.
      (generate-bindings generator)
      (write-bindings generator :in-package in-package))))

(defun eval-bindings (defs)
  "Simply evaluates a list of forms."
  (eval (cons 'progn defs)))



