(defpackage :c2ffi
  (:shadowing-import-from :iterate :collect :collecting)
  (:use :cl :xmls :anaphora :cl-utilities :iterate))
